/* Entity Options: 
  
  "name",
  "id",
  "industry",
  "country",
  "region"
  "type"
  
*/

/* Data Options: 
  
  "SP",
  "FitchLongTermRating", 
  "PDContributionCount",
  "CBRSD",
  "PDSkew",
  "Rating",
  "PD",
  "LGD",
  "EL",
  "PDMedianProxyBps",
  "LGDContributionCount",
  "DepthDecile"
  
*/

export const enabledEntityItems = [
  "name",
  "id",
  "industry",
  "country",
  "region"
];

export const enabledDataItems = [
  "PD",
  "PDMedianProxyBps",
  "Rating",
  "PDContributionCount",
  "SP"
];
