/* Table Options: 
  
  "SP",
  "FitchLongTermRating", 
  "PDContributionCount",
  "CBRSD",
  "PDSkew",
  "Rating",
  "PD",
  "LGD",
  "EL",
  "PDMedianProxyBps",
  "LGDContributionCount",
  "DepthDecile"
  
*/

export const enabledTableItems = [
  "PD",
  "Rating",
  "PDContributionCount",
  "PDMedianProxyBps",
  "LGD",
  "LGDContributionCount"
];
