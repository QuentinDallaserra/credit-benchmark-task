export const IS_GRAPH_ANIMATING = "IS_GRAPH_ANIMATING";
export const SELECTED_DATE = "SELECTED_DATE";
export const SELECTED_PLOT_INDEX = "SELECTED_PLOT_INDEX";

/* Graph ID Options: 

  "PDContributionCount",
  "CBRSD",
  "PD",
  "LGD",
  "EL",
  "PDMedianProxyBps"

*/

export const enabledGraphs = [
  { id: "LGD", label: true },
  { id: "PD", label: false }
];

export const firstGraph = enabledGraphs[0];

export const chartOptions = {
  animationDuration: 500,
  strokeDash: '3 3',
  type: 'monotone',
  primaryColor: '#1F355E',
  secondaryColor: '#7BB8AF',
  tertiaryColor: '#949494',
}
