import React, { Component } from "react";
import { connect } from "react-redux";

import "../scss/index.scss";

import Company from "./views/Company";

class App extends Component {
  render() {
    return <Company />;
  }
}

export default connect()(App);
