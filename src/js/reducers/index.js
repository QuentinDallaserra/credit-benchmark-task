import { combineReducers } from "redux";
import company from "./company";
import graphs from "./graphs";
export default combineReducers({
  company,
  graphs
});
