import {
  FETCH_COMPANY_DATA_BEGIN,
  FETCH_COMPANY_DATA_SUCCESS,
  FETCH_COMPANY_DATA_FAILURE
} from "../constants/company";

const initialState = {
  loading: false,
  error: false,
  entity: null,
  data: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COMPANY_DATA_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_COMPANY_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        entity: action.payload[0].response.entity,
        data: action.payload[0].response.data
      };
    case FETCH_COMPANY_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
        entity: null,
        data: null
      };
    default:
      return state;
  }
};
