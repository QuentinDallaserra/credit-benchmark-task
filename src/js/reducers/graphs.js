import { IS_GRAPH_ANIMATING, SELECTED_DATE, SELECTED_PLOT_INDEX } from "../constants/graphs";

const initialState = {
  isGraphAnimating: false,
  selectedDate: null,
  selectedPlotIndex: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case IS_GRAPH_ANIMATING:
      return {
        ...state,
        isGraphAnimating: action.payload
      };
    case SELECTED_DATE:
      return {
        ...state,
        selectedDate: action.payload
      };
    case SELECTED_PLOT_INDEX:
      return {
        ...state,
        selectedPlotIndex: action.payload
      };
    default:
      return state;
  }
};
