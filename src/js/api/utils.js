import moment from "moment";

export const formatDate = date => moment(date, "YYYYMMDD").format("MMM YY");

export const sortByDate = date => date.sort((a, b) => a.date - b.date);

export const createDataFromStore = (compareArray, object, formatedDate) => {
  return Object.keys(object)
    .filter(key => compareArray.includes(key) || key === "date")
    .reduce((obj, key) => {
      if (formatedDate) {
        return {
          ...obj,
          [key]: object[key],
          formatedDate: formatDate(object.date)
        };
      } else {
        return {
          ...obj,
          [key]: object[key]
        };
      }
    }, {});
};

export const humanizeRule = ruleType => {
  switch (ruleType) {
    case "formatedDate":
      return "Date";
    case "SP":
      return "SP";
    case "FitchLongTermRating":
      return "Fitch Long Term Rating";
    case "PDContributionCount":
      return "PD Contribution Count";
    case "CBRSD":
      return "CBRSD";
    case "PDSkew":
      return "PDSkew";
    case "Rating":
      return "Rating";
    case "PD":
      return "PD";
    case "LGD":
      return "LGD";
    case "EL":
      return "EL";
    case "PDMedianProxyBps":
      return "PD Median Proxy Bps";
    case "LGDContributionCount":
      return "LGD Contribution Count";
    case "DepthDecile":
      return "Depth Decile";
    default:
      return ruleType;
  }
};

export const handleErrors = response => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
