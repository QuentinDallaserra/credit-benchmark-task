import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchData } from "../actions/company";
import { enabledGraphs } from "../constants/graphs";
import { enabledTableItems } from "../constants/tables";
import { enabledEntityItems, enabledDataItems } from "../constants/info";

import Graphs from "../containers/Graphs";
import Info from "../containers/Info";
import Tables from "../containers/Tables";

import Loader from "../components/Loader";
import Section from "../components/Section";
import Grid from "../components/Grid";

const mapStateToProps = state => {
  return {
    loading: state.company.loading,
    error: state.company.error,
    data: state.company.data
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ fetchData }, dispatch) };
};

class Company extends Component {
  componentDidMount() {
    this.props.actions.fetchData();
  }

  render() {
    const { error, loading, data } = this.props;

    if (data && !error && !loading) {
      return (
        <>
          <Section>
            <Grid width={80} flex>
              <Graphs graphs={enabledGraphs} />
            </Grid>
            <Grid width={20}>
              <Info entityData={enabledEntityItems} infoData={enabledDataItems} />
            </Grid>
          </Section>
          <Section>
            <Tables tableData={enabledTableItems} />
          </Section>
        </>
      );
    } else if (error) {
      return <Loader error />;
    } else {
      return <Loader loading />;
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Company);
