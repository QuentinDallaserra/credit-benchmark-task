import { IS_GRAPH_ANIMATING, SELECTED_DATE, SELECTED_PLOT_INDEX } from "../constants/graphs";

export const setGraphAnimationState = isGraphAnimating => dispatch => {
  dispatch({
    type: IS_GRAPH_ANIMATING,
    payload: isGraphAnimating
  });
};

export const selectedDate = date => dispatch => {
  dispatch({
    type: SELECTED_DATE,
    payload: date
  });
};

export const selectedPlotIndex = index => dispatch => {
  dispatch({
    type: SELECTED_PLOT_INDEX,
    payload: index
  });
};
