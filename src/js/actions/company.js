import {
  FETCH_COMPANY_DATA,
  FETCH_COMPANY_DATA_BEGIN,
  FETCH_COMPANY_DATA_SUCCESS,
  FETCH_COMPANY_DATA_FAILURE
} from "../constants/company";

export const fetchData = () => dispatch => {
  dispatch({
    type: FETCH_COMPANY_DATA
  });
};

export const fetchDataBegin = () => dispatch => {
  dispatch({
    type: FETCH_COMPANY_DATA_BEGIN
  });
};

export const fetchDataSuccess = data => dispatch => {
  dispatch({
    type: FETCH_COMPANY_DATA_SUCCESS,
    payload: data
  });
};

export const fetchDataFailure = error => dispatch => {
  dispatch({
    type: FETCH_COMPANY_DATA_FAILURE,
    payload: error
  });
};
