import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { selectedDate, selectedPlotIndex } from "../actions/graphs";
import { createDataFromStore } from "../api/utils";
import Table from "../components/Table";

const mapStateToProps = state => {
  return {
    data: state.company.data,
    selectedDate: state.graphs.selectedDate
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ selectedDate, selectedPlotIndex }, dispatch) };
};

class Tables extends Component {
  state = {
    tableData: []
  };

  generateTableData(enabledTableItems) {
    const tableData = this.props.data.map(row => createDataFromStore(enabledTableItems, row, true));
    this.setState({ tableData });
  }

  handleSelectedDate(event) {
    this.props.actions.selectedDate(event);
  }

  handleSelectedPlotIndex(event) {
    this.props.actions.selectedPlotIndex(event);
  }

  componentDidMount() {
    this.generateTableData(this.props.tableData);
  }

  render() {
    const { tableData } = this.state;
    const { selectedDate } = this.props;

    return tableData && tableData.length
      ? <Table 
          tableData={tableData}
          setSelectedDate={event => this.handleSelectedDate(event)}
          setSelectedPlotIndex={event => this.handleSelectedPlotIndex(event)}
          selectedDate={selectedDate}
        />
      : <></>;
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tables);
