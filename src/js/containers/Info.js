import React, { Component } from "react";
import { connect } from "react-redux";
import { createDataFromStore } from "../api/utils";
import Entity from "../components/Entity";
import InfoWidget from "../components/InfoWidget";

const mapStateToProps = state => {
  return {
    entity: state.company.entity,
    data: state.company.data,
    selectedDate: state.graphs.selectedDate
  };
};

class Info extends Component {
  state = {
    entityData: {},
    infoData: []
  };

  generateCompanyEntityData(enabledEntityItems) {
    const entityData = createDataFromStore(enabledEntityItems, this.props.entity);

    this.setState({ entityData });
  }

  generateCompanyInfoData(enabledDataItems) {
    const infoData = this.props.data.map(row => createDataFromStore(enabledDataItems, row));

    this.setState({ infoData });
  }

  componentDidMount() {
    this.generateCompanyEntityData(this.props.entityData);
    this.generateCompanyInfoData(this.props.infoData);
  }

  render() {
    const { entityData, infoData } = this.state;
    const { selectedDate } = this.props;

    return (
      <>
        {entityData && <Entity entityData={entityData} />}
        {infoData && infoData.length &&
          <InfoWidget
            infoData={infoData}
            selectedDate={selectedDate}
          />
        }
      </>
    );
  }
}

export default connect(
  mapStateToProps,
  null
)(Info);
