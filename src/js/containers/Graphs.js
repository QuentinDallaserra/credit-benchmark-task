import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { formatDate, sortByDate, humanizeRule } from "../api/utils";
import { firstGraph, chartOptions } from "../constants/graphs";
import { selectedDate, setGraphAnimationState, selectedPlotIndex } from "../actions/graphs";
import Button from "../components/Button";
import Graph from "../components/Graph";

const mapStateToProps = state => {
  return {
    data: state.company.data,
    isGraphAnimating: state.graphs.isGraphAnimating,
    selectedPlotIndex: state.graphs.selectedPlotIndex
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ 
    selectedDate,
    setGraphAnimationState,
    selectedPlotIndex },
  dispatch) };
};

class Graphs extends Component {
  
  state = {
    graphs: [],
    currentGraphID: firstGraph.id,
    activeTooltip: null
  };

  generateGraphs(enabledGraphs) {
    const graphs = enabledGraphs.map(graph => ({
      id: graph.id,
      label: graph.label,
      plots: sortByDate(this.props.data.map(plot => ({ 
        date: plot.date,
        value: plot[graph.id] || 0,
        formatedDate: formatDate(plot.date)
      })))
    }));

    this.setState({ graphs });
  }

  setCurrentGraph(id) {
    this.setState({ currentGraphID: id });
  }

  handleSelectedDate(event) {
    this.props.actions.selectedDate(event);
  }

  handleGraphAnimationState(event) {
    this.props.actions.setGraphAnimationState(event);
  }

  handleSelectedPlotIndex(event) {
    this.props.actions.selectedPlotIndex(event);
  }

  resetActiveStyles = (dots, lines) => {
    dots.forEach(dot => {
      dot.style.fill = "#FFF";
    });
    lines.forEach(line => {
      line.style.strokeDasharray = chartOptions.strokeDash;
      line.style.stroke = chartOptions.secondaryColor;
    });
  };

  selectActiveCircle = (index) => {
    if (!index) {
      return;
    }

    const dots = document.querySelectorAll(".recharts-line-dot");
    const lines = document.querySelector(".recharts-cartesian-grid-vertical").childNodes;

    if (this.props.isGraphAnimating) {
      this.resetActiveStyles(dots, lines);
      return;
    }

    this.resetActiveStyles(dots, lines);

    dots[index].style.fill = chartOptions.primaryColor;
    lines[index].style.strokeDasharray = "0";
    lines[index].style.stroke = chartOptions.tertiaryColor;
  };

  componentDidMount() {
    this.generateGraphs(this.props.graphs);
  }

  componentDidUpdate() {
    this.selectActiveCircle(this.props.selectedPlotIndex);
  }

  render() {
    const { graphs, currentGraphID } = this.state;
    const currentGraph = graphs.find(graph => graph.id === currentGraphID);

    return (
      <>
        {currentGraph && <Graph 
          plots={currentGraph.plots}
          yLabel={currentGraph.label}
          date={currentGraph.date}
          setGraphAnimationState={event => this.handleGraphAnimationState(event)}
          setSelectedPlotIndex={event => this.handleSelectedPlotIndex(event)}
          setSelectedDate={event => this.handleSelectedDate(event)}
        />}

        {graphs.map(graph => (
          
          <Button 
            key={`button-graph-${graph.id}`}
            active={currentGraphID === graph.id}
            label={humanizeRule(graph.id)}
            onClick={() => {
              this.setCurrentGraph(graph.id)
            }}
          />
        ))}
      </>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Graphs);
