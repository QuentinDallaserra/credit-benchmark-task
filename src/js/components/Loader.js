import React from "react";
import PropTypes from "prop-types";

const Loader = ({ error, loading }) => {
  return (
    <div className="loader">
      {error && <div className="loader__error">error loading data</div>}
      {loading && (
        <div className="loader__ring">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      )}
    </div>
  );
};

Loader.propTypes = { 
  error: PropTypes.bool,
  loading: PropTypes.bool,
};

export default Loader;
