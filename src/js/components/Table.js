import React from "react";
import { humanizeRule } from "../api/utils";

const Table = ({ tableData, setSelectedDate, setSelectedPlotIndex, selectedDate }) => {
  return (
    <table className="table">
      <thead>
        <tr className="table__row">
          {Object.keys(tableData[0])
            .filter(key => key !== "date")
            .map(th => (
              <th className="table__header" key={`table-header-${th}`}>{humanizeRule(th)}</th>
            ))}
        </tr>
      </thead>

      <tbody>
        {tableData.map((tableItem, index) => (
          <tr 
            onMouseOver={() => {
              setSelectedDate(tableItem.date);
              // HACK Reversed array because lack of time.
              // Ideally I would use global store date sorting and copare to current object index
              setSelectedPlotIndex(tableData.length - index - 1);
            }}
            key={`table-row-${tableItem}-${index}`}
            className={ `table__row ${tableItem.date === selectedDate && 'table__row--active'}`}
          >
            {Object.keys(tableItem)
              .filter(key => key !== "date")
              .map((key, index) => {
                return <td 
                        className={ `table__data ${!tableItem[key] && 'table__data--inactive'}`}
                        key={`table-data-${tableItem[key] || index}`}>{tableItem[key] || "No data"}
                      </td>;
              })}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
