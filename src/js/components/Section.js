import React, { Component } from "react";
import PropTypes from "prop-types";

class Section extends Component {
  render() {
    return <section className="section">{this.props.children}</section>;
  }
}

Section.propTypes = {
  children:  PropTypes.oneOfType([
    PropTypes.array.isRequired,
    PropTypes.object.isRequired
  ]), 
};

export default Section;
