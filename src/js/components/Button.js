import React from "react";
import PropTypes from "prop-types";

const Button = ({ label, active, onClick }) => {
  return (
    <button className={`button ${active ? "button--active" : ''}`} onClick={onClick}>
      {label}
    </button>
  );
};

Button.propTypes = { 
  label: PropTypes.string.isRequired,
  active: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

Button.defaultProps = {
  active: false,
};

export default Button;
