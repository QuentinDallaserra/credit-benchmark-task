import React from "react";
import { ResponsiveContainer, LineChart, Line, CartesianGrid, XAxis, YAxis } from "recharts";
import PropTypes from "prop-types";
import { chartOptions } from "../constants/graphs";

const Graph = ({plots, yLabel, setSelectedDate, setSelectedPlotIndex, setGraphAnimationState}) => {
  return (
    <ResponsiveContainer width="100%" height={400}>
      <LineChart
        data={plots}
        onMouseMove={event => {
          setSelectedPlotIndex(event && event.activeTooltipIndex);
          setSelectedDate(event && event.activePayload[0].payload.date);
        }}
      >
        <Line 
          onAnimationStart={() => setGraphAnimationState(true)}
          onAnimationEnd={() => setGraphAnimationState(false)}
          animationDuration={chartOptions.animationDuration}
          type={chartOptions.type}
          dataKey="value"
          stroke={chartOptions.primaryColor}
        />
        <CartesianGrid strokeDasharray={chartOptions.strokeDash} stroke={chartOptions.secondaryColor} />
        <XAxis dataKey="formatedDate" stroke={chartOptions.secondaryColor} />
        <YAxis tick={yLabel} stroke={chartOptions.secondaryColor} />
      </LineChart>
    </ResponsiveContainer>
  );
};

Graph.propTypes = {
  plots: PropTypes.array.isRequired,
  yLabel: PropTypes.bool,
  selectActiveCircle: PropTypes.func,
  handleSelectedDate: PropTypes.func
};

Graph.defaultProps = {
  yLabel: false,
  selectActiveCircle: () => {},
  handleSelectedDate: () => {}
};

export default Graph;
