import React, { Component } from "react";
import PropTypes from "prop-types";

class Grid extends Component {
  render() {
    const { children, width, flex } = this.props;
    return <section className={`grid grid--${width} ${flex ? "grid--flex" : ''}`}>{children}</section>;
  }
}

Grid.propTypes = {
  children:  PropTypes.oneOfType([
    PropTypes.array.isRequired,
    PropTypes.object.isRequired
  ]), 
  width: PropTypes.number,
  flex: PropTypes.bool
};

Grid.defaultProps = {
  width: 100,
  flex: false
};

export default Grid;
