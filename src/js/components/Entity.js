import React from "react";
import PropTypes from "prop-types";

const Entity = ({ entityData }) => {
  return <div className="container entity">
    {Object.keys(entityData).map((key, index) => {
      return <h3 key={`entity-${entityData[key]}-${index}`} className="entity__title">{entityData[key]}</h3>
    })}
  </div>;
};

Entity.propTypes = { 
  entityData: PropTypes.object.isRequired,
};

export default Entity;
