import React from "react";
import PropTypes from "prop-types";
import { humanizeRule } from "../api/utils";

const infoRows = data => {
  return Object.entries(data).filter(([key]) => key !== "date").map(([key, value], index) => (
    <div className="info-widget__row" key={`info-row-${key}-${index}`}>
      <h3 className="info-widget__title">{humanizeRule(key)}:</h3> <div className="info-widget__value">{value || "-"}</div>
    </div>
  ));
};

const InfoWidget = ({ infoData, selectedDate }) => {
  return <div className="container info-widget">{selectedDate 
           ? infoData.filter(data => data.date === selectedDate).map(item => infoRows(item))
           : infoRows(infoData[0])}
         </div>;
};

InfoWidget.propTypes = { 
  infoData: PropTypes.array.isRequired,
  selectedDate: PropTypes.number,
};

InfoWidget.defaultProps = {
  selectedDate: 0,
};


export default InfoWidget;
