import { FETCH_COMPANY_DATA, dataURL } from "../constants/company";
import { fetchDataBegin, fetchDataSuccess, fetchDataFailure } from "../actions/company";
import { handleErrors } from "../api/utils";

export default store => next => action => {
  switch (action.type) {
    case FETCH_COMPANY_DATA:
      store.dispatch(fetchDataBegin());

      fetch(dataURL)
        .then(handleErrors)
        .then(response => response.json())
        .then(data =>
          setTimeout(() => { // Simulate slow response
            store.dispatch(fetchDataSuccess(data));
          }, 2000)
        )
        .catch(() =>
          setTimeout(() => { // Simulate slow response
            store.dispatch(fetchDataFailure(true));
          }, 2000)
        );
      return next(action);
    default:
      return next(action);
  }
};
